// This is extremely terrible and hacky, but I did not want to set up a full
// build environment to use an actual XML parser when I originally wrote this
export default {
  async fetch(request, env) {
    console.log(request);
    const url = new URL(request.url);
    if (url.pathname.toLowerCase() === '/autodiscover/autodiscover.xml') {
      if (request.method === 'POST') {
        const reqBody = await request.text();
        const loginName = [...reqBody.matchAll(/<EMailAddress>(.*)<\/EMailAddress>/gi)][0][1];
        if (loginName.includes('<') || loginName.includes('>') || loginName.includes('\'') || loginName.includes('"'))
          throw new Error('Invalid EMailAddress');
        const resBody = `<?xml version="1.0" encoding="utf-8" ?>\
<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/responseschema/2006">\
<Response xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a">\
<Account>\
<AccountType>email</AccountType>\
<Action>settings</Action>\
<Protocol>\
<Type>IMAP</Type>\
<Server>mbox.cert.legalmail.it</Server>\
<Port>993</Port>\
<DomainRequired>off</DomainRequired>\
<SPA>off</SPA>\
<SSL>on</SSL>\
<AuthRequired>on</AuthRequired>\
<LoginName>${loginName}</LoginName>\
</Protocol>\
<Protocol>\
<Type>POP3</Type>\
<Server>mbox.cert.legalmail.it</Server>\
<Port>995</Port>\
<DomainRequired>off</DomainRequired>\
<SPA>off</SPA>\
<SSL>on</SSL>\
<AuthRequired>on</AuthRequired>\
<LoginName>${loginName}</LoginName>\
</Protocol>\
<Protocol>\
<Type>SMTP</Type>\
<Server>sendm.cert.legalmail.it</Server>\
<Port>465</Port>\
<DomainRequired>off</DomainRequired>\
<SPA>off</SPA>\
<SSL>on</SSL>\
<AuthRequired>on</AuthRequired>\
<LoginName>${loginName}</LoginName>\
</Protocol>\
</Account>\
</Response>\
</Autodiscover>`;
        return new Response(resBody, {
          status: 200,
          headers: new Headers({
            'Content-Type': 'text/xml',
            'Content-Length': resBody.length
          })
        });
      } else {
        return new Response(null, {
          status: 405,
          headers: new Headers({
            Allow: 'POST'
          })
        });
      }
    } else {
      return await env.ASSETS.fetch();
    }
  },
};