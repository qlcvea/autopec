# autopec

An attempt to set up automatic configuration of my
[InfoCert/Legalmail](https://www.legalmail.infocert.it/)
[PEC](https://www.agid.gov.it/en/platforms/certified-email) mailbox in
Thunderbird and Outlook using [Cloudflare Pages](https://pages.cloudflare.com/).

Currently deployed at <https://pec.qlcvea.com>.
- Thunderbird autoconfig:
  <https://pec.qlcvea.com/.well-known/autoconfig/mail/config-v1.1.xml>
- Outlook autodiscover:
  <https://pec.qlcvea.com/autodiscover/autodiscover.xml> (and some capitalized
  variants, see `content/_routes.json`. An autodiscover POST request is required
  to get a response, see `example_autodiscover_req.xml`.)

Thunderbird is working.

Outlook is...strange.  
This autodiscover setup will NOT work when adding an account using the new,
"modern" UI (the "For MS365 and 2016, 2019, or 2021" dialog shown on
[this page](https://support.microsoft.com/en-us/office/add-an-email-account-to-outlook-6e27792a-9267-4aa4-8bb6-c84ef146101b#PickTab=Outlook_for_Windows))
that is used in the latest versions of Outlook.  
However, it WILL work if you use the "old" version of the account setup. In
newer versions of Outlook, this flow can be started by adding an email account
from Control Panel ("Mail (Microsoft Outlook)") instead of using the Outlook app
directly.

These autoconfigurations also fail to account for InfoCert's folder structure.  
The default folder names are in Italian ("Bozze", "Spedite", etc.) and they are
under the "INBOX" folder.
